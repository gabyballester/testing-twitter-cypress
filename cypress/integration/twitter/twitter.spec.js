// Contexto engloba todo el test que dentro tiene dos pruebas
context('Navegación por google', () => { //El nombre del objetivo lo definimos aquí

    // Con 'it' definimos una prueba y damos nombre a la misma
    it('Visitar la página de Google', () => {
        //Con la palabra clave cy y el metodo visit y la página a visitar
        cy.visit('https://www.google.com/');
    });

    it('Búsqueda de Twitter', () => {
        //selector del input de google
        cy.get('.gLFyf').type('Twitter'); //Navegador introduce palabra en el input
    });

    it('Hacemos click en botón de búsqueda de google', () => {
        cy.get('.aajZCb > .tfB0Bf > center > .gNO89b').click();
    });

    it('Hacer click primer enlace y entrar en la web', () => {
        // Hacemos click en el primer elemento de la búsqueda
        cy.get('[href="https://twitter.com/?lang=es"] > .LC20lb').click();
    });

});